
The content of this repository is (mostly) binary files downloaded
from publicly accessible sites on the Internet.

This primarily serves as an archive for the content used
in [`common-salt-states`][1] repository.

The files are placed into paths predefined in configuration
for automated deployment.

[1]: https://github.com/uvsmtid/common-salt-states

